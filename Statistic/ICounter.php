<?php

namespace Statistic;

interface ICounter
{
    /**
     * @param string $id
     */
    public function increaseByOne(string $id): void;
}