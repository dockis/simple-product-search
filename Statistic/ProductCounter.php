<?php

namespace Statistic;

use DataProvider\ICounterDriver;

class ProductCounter implements ICounter
{
    /**
     * @var ICounterDriver
     */
    private $counterDriver;

    /**
     * @param ICounterDriver $counterDriver
     */
    public function __construct(ICounterDriver $counterDriver)
    {
        $this->counterDriver = $counterDriver;
    }

    /**
     * @param string $productId
     */
    public function increaseByOne(string $productId): void
    {
        $this->setCountById($productId, $this->getCountById($productId) + 1);
    }

    /**
     * @param string $productId
     * @return int|null
     */
    private function getCountById(string $productId): int
    {
        return $this->counterDriver->getCountById($productId);
    }

    /**
     * @param string $productId
     * @param int $productCount
     */
    private function setCountById(string $productId, int $productCount): void
    {
        $this->counterDriver->setCountById($productId, $productCount);
    }
}