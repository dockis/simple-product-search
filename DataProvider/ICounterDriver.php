<?php

namespace DataProvider;

interface ICounterDriver
{
    /**
     * @param string $id
     * @return int
     */
    public function getCountById(string $id): int;

    /**
     * @param string $id
     * @param int $count
     */
    public function setCountById(string $id, int $count): void;
}