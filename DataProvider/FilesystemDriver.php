<?php

namespace DataProvider;

class FilesystemDriver implements IFilesystemDriver
{
    /**
     * @param string $filename
     * @return string
     */
    public function getFileContent(string $filename): string
    {
        // TODO: Implement getFileContent() method.
    }

    /**
     * @param string $filename
     * @param string $fileContent
     */
    public function setFileContent(string $filename, string $fileContent): void
    {
        // TODO: Implement setFileContent() method.
    }
}