<?php

namespace DataProvider;

interface IFilesystemDriver
{
    /**
     * @param string $filename
     * @return string
     */
    public function getFileContent(string $filename): string;

    /**
     * @param string $filename
     * @param string $fileContent
     */
    public function setFileContent(string $filename, string $fileContent): void;
}