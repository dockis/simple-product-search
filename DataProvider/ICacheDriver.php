<?php

namespace DataProvider;

interface ICacheDriver
{
    /**
     * @param string $id
     * @return array|null
     */
    public function findProduct(string $id): ?array;

    /**
     * @param $product[]
     */
    public function addProduct(array $product): void;
}