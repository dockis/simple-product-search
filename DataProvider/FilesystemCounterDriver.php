<?php

namespace DataProvider;

class FilesystemCounterDriver implements ICounterDriver
{
    /**
     * @var IFilesystemDriver
     */
    private $filesystemDriver;
    /**
     * @var string
     */
    private $filename;

    /**
     * @param IFilesystemDriver $filesystemDriver
     * @param string $filename
     */
    public function __construct(IFilesystemDriver $filesystemDriver, string $filename)
    {
        $this->filesystemDriver = $filesystemDriver;
        $this->filename = $filename;
    }

    /**
     * @param string $id
     * @return int
     */
    public function getCountById(string $id): int
    {
        $counter = $this->getCounter($this->filename);

        return isset($counter[$id]) ? $counter[$id] : 0;
    }

    /**
     * @param string $id
     * @param int $count
     */
    public function setCountById(string $id, int $count): void
    {
        $counter = $this->getCounter($this->filename);
        $counter[$id] = $count;
        $this->setCounter($this->filename, $counter);
    }

    /**
     * @param string $filename
     * @return array
     */
    private function getCounter(string $filename): array
    {
        $counter = json_decode($this->filesystemDriver->getFileContent($filename));

        return $counter ?: [];
    }

    /**
     * @param string $filename
     * @param $counter []
     */
    private function setCounter(string $filename, array $counter): void
    {
        $this->filesystemDriver->setFileContent($filename, json_encode($counter));
    }
}