<?php

namespace Controller\ProductController;

use Manager\Product as ProductManager;

class ProductController
{
    /**
     * @var ProductManager
     */
    private $productManager;

    /**
     * @param ProductManager $productManager
     */
    public function __construct(ProductManager $productManager)
    {
        $this->productManager = $productManager;
    }

    /**
     * @param string $id
     * @return string
     */
    public function detail(string $id): string
    {
        return json_encode($this->productManager->productDetail($id));
    }
}