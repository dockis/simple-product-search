<?php

namespace Manager;

use DataProvider\ICacheDriver;
use DataProvider\IElasticSearchDriver;
use DataProvider\IMySQLDriver;
use Statistic\ICounter;
use Statistic\ProductCounter;

class Product
{
    /**
     * @var ICacheDriver
     */
    private $cacheDriver;

    /**
     * @var IElasticSearchDriver
     */

    private $elasticSearchDriver;

    /**
     * @var IMySQLDriver
     */
    private $mySQLDriver;

    /**
     * @var ProductCounter
     */
    private $productCounter;

    /**
     * @param ICacheDriver $cacheDriver
     * @param IElasticSearchDriver $elasticSearchDriver
     * @param IMySQLDriver $mySQLDriver
     * @param ICounter $productCounter
     */
    public function __construct(
        ICacheDriver $cacheDriver,
        IElasticSearchDriver $elasticSearchDriver,
        IMySQLDriver $mySQLDriver,
        ICounter $productCounter
    ) {
        $this->cacheDriver = $cacheDriver;
        $this->elasticSearchDriver = $elasticSearchDriver;
        $this->mySQLDriver = $mySQLDriver;
        $this->productCounter = $productCounter;
    }

    /**
     * @param string $id
     * @return array
     */
    public function productDetail(string $id): array
    {
        $product = $this->getProductFromCache($id);
        if (!$product) {
            $product = $this->getProductFromDB($id);
            $this->addProductToCache($product);
        }
        $this->productCounter->increaseByOne($id);

        return $product;
    }

    /**
     * @param string $id
     * @return array|null
     */
    private function getProductFromCache(string $id): ?array
    {
        return $this->cacheDriver->findProduct($id);
    }

    /**
     * @param $product []
     */
    private function addProductToCache(array $product): void
    {
        $this->cacheDriver->addProduct($product);
    }

    /**
     * @param string $id
     * @return array
     */
    private function getProductFromDB(string $id): array
    {
        $product = $this->elasticSearchDriver->findById($id);
        if (!$product) {
            $product = $this->mySQLDriver->findProduct($id);
        }

        return $product;
    }
}